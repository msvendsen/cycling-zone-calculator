//
//  SecondViewController.m
//  Cycling Zone Calculator
//
//  Created by Michael svendsen on 11/18/2013.
//  Copyright (c) 2013 mcs-web. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self hrmSliderValueChange:_hrmSlider];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (IBAction)hrmSliderValueChange:(UISlider *)sender {
    
    int intValue = (int)sender.value;
    
    int zoneOneValue = (int)(intValue * 0.68);
    int zoneTwoLowValue = zoneOneValue + 1;
    int zoneTwoHighValue = (int)(intValue * 0.83);
    int zoneThreeLowValue = zoneTwoHighValue + 1;
    int zoneThreeHighValue = (int)(intValue * 0.94);
    int zoneFourLowValue = zoneThreeHighValue + 1;
    int zoneFourHighValue = (int)(intValue * 1.05);
    int zoneFiveValue = zoneFourHighValue + 1;
    
    [self populateAverageValueField:intValue];
    [self populateLTHRValueField:intValue];
    [self populateZoneOne:zoneOneValue];
    [self populateZoneTwo:zoneTwoLowValue withHigh:zoneTwoHighValue];
    [self populateZoneThree:zoneThreeLowValue withHigh:zoneThreeHighValue];
    [self populateZoneFour:zoneFourLowValue withHigh:zoneFourHighValue];
    [self populateZoneFive:zoneFiveValue];
    
}

- (void)populateAverageValueField:(int)value{
    _averageHRMField.text = [NSString stringWithFormat:@"%dbpm", value];
}

- (void)populateLTHRValueField:(int)value{
    _lthrField.text = [NSString stringWithFormat:@"%dbpm", value];
}

- (void) populateZoneOne:(int)lowValue {
    _zoneOneHigh.text = [NSString stringWithFormat:@"< %dbpm", lowValue];
}

- (void)populateZoneTwo:(int)valueLow withHigh:(int)valueHigh{
    _zoneTwoLow.text = [NSString stringWithFormat:@"%dbpm", valueLow];
    _zoneTwoHigh.text = [NSString stringWithFormat:@"%dbpm", valueHigh];
}

- (void)populateZoneThree:(int)valueLow withHigh:(int)valueHigh{
    _zoneThreeLow.text = [NSString stringWithFormat:@"%dbpm", valueLow];
    _zoneThreeHIgh.text = [NSString stringWithFormat:@"%dbpm", valueHigh];
}

- (void)populateZoneFour:(int)valueLow withHigh:(int)valueHigh{
    _zoneFourLow.text = [NSString stringWithFormat:@"%dbpm", valueLow];
    _zoneFourHigh.text = [NSString stringWithFormat:@"%dbpm", valueHigh];
}

- (void) populateZoneFive:(int)lowValue {
    _zoneFiveLow.text = [NSString stringWithFormat:@"%dbpm >", lowValue];
}

@end
