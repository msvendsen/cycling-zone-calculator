//
//  FirstViewController.h
//  Cycling Zone Calculator
//
//  Created by Michael svendsen on 11/18/2013.
//  Copyright (c) 2013 mcs-web. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstViewController : UIViewController


@property (weak, nonatomic) IBOutlet UISlider *wattSlider;
@property (weak, nonatomic) IBOutlet UITextField *averageValueField;
@property (weak, nonatomic) IBOutlet UITextField *ftpValueField;
@property (weak, nonatomic) IBOutlet UITextField *zoneOneHighValueField;
@property (weak, nonatomic) IBOutlet UITextField *zoneTwoLowValueField;
@property (weak, nonatomic) IBOutlet UITextField *zoneTwoHighValueField;
@property (weak, nonatomic) IBOutlet UITextField *zoneThreeLowValueField;
@property (weak, nonatomic) IBOutlet UITextField *zoneThreeHighValueField;
@property (weak, nonatomic) IBOutlet UITextField *zoneFourLowValueField;
@property (weak, nonatomic) IBOutlet UITextField *zoneFourHighValueField;
@property (weak, nonatomic) IBOutlet UITextField *zoneFiveLowValueField;
@property (weak, nonatomic) IBOutlet UITextField *zoneFiveHighValueField;
@property (weak, nonatomic) IBOutlet UITextField *zoneSixLowValueField;




@end
