//
//  AppDelegate.h
//  Cycling Zone Calculator
//
//  Created by Michael svendsen on 11/18/2013.
//  Copyright (c) 2013 mcs-web. All rights reserved.
//

#import <UIKit/UIKit.h>

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
