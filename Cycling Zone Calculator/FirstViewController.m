//
//  FirstViewController.m
//  Cycling Zone Calculator
//
//  Created by Michael svendsen on 11/18/2013.
//  Copyright (c) 2013 mcs-web. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self onFTPSliderValueChanged:_wattSlider];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onFTPSliderValueChanged:(UISlider *)sender {
    
    int intValue = (int)sender.value;
    float ftpValue = (int)(float)intValue * 0.95;
    
    int zone1High = ftpValue * .55;
    
    int zone2Low = zone1High + 1;
    int zone2High = ftpValue * .75;
    
    int zone3Low = zone2High + 1;
    int zone3High = ftpValue * 0.9;
    
    int zone4Low = zone3High + 1;
    int zone4High = ftpValue * 1.05;
    
    int zone5Low = zone4High + 1;
    int zone5High = ftpValue * 1.2;
    
    int zone6Low = zone5High + 1;
    
    
    [self populateAverageValueField:intValue];
    [self populateFTPValueField:ftpValue];
    [self populateZoneOne:zone1High];
    [self populateZoneTwo:zone2Low withHigh:zone2High];
    [self populateZoneThree:zone3Low withHigh:zone3High];
    [self populateZoneFour:zone4Low withHigh:zone4High];
    [self populateZoneFive:zone5Low withHigh:zone5High];
    [self populateZoneSix:zone6Low];
    
    
}

- (void)populateAverageValueField:(int)value{
    _averageValueField.text = [NSString stringWithFormat:@"%dw", value];
}

- (void)populateFTPValueField:(int)value{
    _ftpValueField.text = [NSString stringWithFormat:@"%dw", value];
}

- (void)populateZoneOne:(int)valueHigh{
    _zoneOneHighValueField.text = [NSString stringWithFormat:@"< %dw", valueHigh];
}

- (void)populateZoneTwo:(int)valueLow withHigh:(int)valueHigh{
    _zoneTwoLowValueField.text = [NSString stringWithFormat:@"%dw", valueLow];
    _zoneTwoHighValueField.text = [NSString stringWithFormat:@"%dw", valueHigh];
}

- (void)populateZoneThree:(int)valueLow withHigh:(int)valueHigh{
    _zoneThreeLowValueField.text = [NSString stringWithFormat:@"%dw", valueLow];
    _zoneThreeHighValueField.text = [NSString stringWithFormat:@"%dw", valueHigh];
}

- (void)populateZoneFour:(int)valueLow withHigh:(int)valueHigh{
    _zoneFourLowValueField.text = [NSString stringWithFormat:@"%dw", valueLow];
    _zoneFourHighValueField.text = [NSString stringWithFormat:@"%dw", valueHigh];
}

- (void)populateZoneFive:(int)valueLow withHigh:(int)valueHigh{
    _zoneFiveLowValueField.text = [NSString stringWithFormat:@"%dw", valueLow];
    _zoneFiveHighValueField.text = [NSString stringWithFormat:@"%dw", valueHigh];
}

- (void)populateZoneSix:(int)valueLow{
    _zoneSixLowValueField.text = [NSString stringWithFormat:@"%dw >", valueLow];
}


@end
