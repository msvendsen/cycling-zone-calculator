//
//  SecondViewController.h
//  Cycling Zone Calculator
//
//  Created by Michael svendsen on 11/18/2013.
//  Copyright (c) 2013 mcs-web. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController

@property (weak, nonatomic) IBOutlet UISlider *hrmSlider;
@property (weak, nonatomic) IBOutlet UITextField *averageHRMField;
@property (weak, nonatomic) IBOutlet UITextField *lthrField;
@property (weak, nonatomic) IBOutlet UITextField *zoneOneHigh;
@property (weak, nonatomic) IBOutlet UITextField *zoneTwoLow;
@property (weak, nonatomic) IBOutlet UITextField *zoneTwoHigh;
@property (weak, nonatomic) IBOutlet UITextField *zoneThreeLow;
@property (weak, nonatomic) IBOutlet UITextField *zoneThreeHIgh;
@property (weak, nonatomic) IBOutlet UITextField *zoneFourLow;
@property (weak, nonatomic) IBOutlet UITextField *zoneFourHigh;
@property (weak, nonatomic) IBOutlet UITextField *zoneFiveLow;

@end
